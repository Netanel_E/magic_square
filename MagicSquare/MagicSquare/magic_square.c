#include "magic_square.h"


bool is_diag_sum_equal(int *square, uint32_t size, int *left_diag)
{
	int right_diag = 0;

	// sum the square diagoanls
	for (uint32_t i = 0; i < size; ++i)
	{
		*left_diag += square[i + size * i];
		right_diag += square[(size - 1) + (size - 1) * i];
	}
	// check if the diagonals have the same sum
	if (*left_diag != right_diag)
	{
		return false;
	}
	return true;
}


bool is_cols_and_rows_sum_equal(int* square, uint32_t size, int left_diag)
{
	int row = 0;
	int col = 0;

	for (uint32_t i = 0; i < size; ++i)
	{
		// sum the square row and col in each itaration
		for (uint32_t j = 0; j < size; ++j)
		{
			row += square[(i * size) + j];
			col += square[(j * size) + i];
		}
		// check if the row and col have the same sum as diagonals sum
		if ((row != left_diag) | (col != left_diag))
		{
			return false;
		}
		row = 0;
		col = 0;
	}
	return true;
}


bool is_magic_square(int *square, uint32_t size)
{

	int left_diag = 0;

	// sum the square diagoanls and check if equal
	if (!is_diag_sum_equal(square, size, &left_diag))
	{
		return false;
	}

	// sum the square rows and cols and check if equal to diag sum
	if (!is_cols_and_rows_sum_equal(square, size, left_diag))
	{
		return false;
	}

	return true;
}
