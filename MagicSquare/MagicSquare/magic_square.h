#pragma once

#include <stdlib.h>
#include <stdbool.h>
#include <errno.h>
#include <inttypes.h>


/**
* This function validates if a given nxn square is a magic square.
*
* @param square - nxn array filled with int values
* @param size - size n of the 2D array
*
* @return True if is a magic square
*	False if not a magic square
*/
bool is_magic_square(int * square, uint32_t size);

/**
* This function validates if diagonals of given nxn square has the same sum.
*
* @param square - nxn array filled with int values
* @param size - size n of the 2D array
* @param left_diag - sum of the left diagonal (by pointer)
*
* @return True if has the same sum
*	False if doesn't
*/
bool is_diag_sum_equal(int* square, uint32_t size, int* left_diag);

/**
* This function validates if rows and cols of given nxn square has the same sum as diagonals sum.
*
* @param square - nxn array filled with int values
* @param size - size n of the 2D array
* @param left_diag - sum of the left diagonal (by pointer)
*
* @return True if has the same sum
*	False if doesn't
*/
bool is_cols_and_rows_sum_equal(int* square, uint32_t size, int left_diag);
