#include <stdio.h>
#include "magic_square.h"

#define SIZE (3)


int main()
{
	int arr[SIZE][SIZE] = {{4, 3, 8}, {9, 5, 1}, {2, 7, 6}};
	// allocate memory to copy the 2D array
	int* square = malloc(SIZE * SIZE * sizeof(square));
	if (NULL == square)
	{
		perror("malloc");
		return EXIT_FAILURE;
	}
	// copy the array to our allocated memory (to check our function)
	int i, j = 0;
	for (i = 0; i < SIZE; i++)
		for (j = 0; j < SIZE; j++)
			*(square + i*SIZE + j) = arr[i][j];

	uint32_t size = sizeof(*arr)/sizeof(int);

	printf(is_magic_square(square, size) ? "true\n" : "false\n");

	return EXIT_SUCCESS;
}
